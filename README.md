# homepage

Forked from: https://github.com/hidanoki/homepage

I made some small changes, like being able to directly type a website url into the search bar, a greeting, the date and time. You can also hover some link so it shows website subdomains and I changed some search prefixes.


Theres a search bar at the bottom that uses prefixes such as !g (google) or !y (youtube) to search different websites (code from twily: http://twily.info/firefox/). These can be edited in the .js file.

Available search websites and their prefixes:

    Default: DuckDuckGo
    !g: Google
    !i: Google Images
    !y: Youtube
    !w: Wikipedia
    !t: Google Translate
    !r: Reddit search
    !s: Subreddit
    !u: Urban Dictionary
    !b: Bing Images (since Google removed View Image feature)